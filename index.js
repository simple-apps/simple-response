const express = require("express");
const app = express();
const port = 3000;

function message() {
  return `Simple response app -- listening on port ${port} -- the time is ${new Date(
    Date.now()
  ).toUTCString()}`;
}

app.get("/", (req, res) => res.send(message()));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
